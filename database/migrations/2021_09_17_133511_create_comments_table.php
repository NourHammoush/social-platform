<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();

            $table->string('body');

            $table->unsignedBigInteger('parent_id');
            $table->foreign('parent_id')
                ->references('id')->on('comments')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->enum('level', [0, 1, 2, 3, 4]);
            // $table->tinyInteger('level');

            $table->unsignedBigInteger('post_id');
            $table->foreign('post_id')
                ->references('id')->on('posts')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('commentable_id');
            $table->string('commentable_type');
            $table->index(['commentable_id', 'commentable_type']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
