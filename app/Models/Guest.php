<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function posts()
    {
        return $this->morphMany(Post::class, 'postable');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
