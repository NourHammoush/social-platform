<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    public function imagable()
    {
        return $this->morphTo();
    }
    /**
     * Get all of the posts that are assigned this image.
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'imagable');
    }

    /**
     * Get all of the comments that are assigned this image.
     */
    public function comments()
    {
        return $this->morphedByMany(Comment::class, 'imagable');
    }
}
